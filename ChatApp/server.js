/**
 * Created by Farhad Zaman on 2/13/2017.
 */
"use strict";
const express = require("express");
const app = express();
const server = require("http").createServer(app);
const io = require("socket.io").listen(server, {'pingTimeout': 2000, 'pingInterval': 1000});
const jwt_decode = require("jwt-decode");
const jwt = require('jwt-simple');
const mysql = require("mysql");
const moment = require('moment');
const database = require('./databaseConfig');
const extract = require('meta-extractor');
const getUrl = require('get-urls');
const {URL} = require('url');
const url2 = require('url');
const group = require("./group");
const mysql2 = require("mysql2/promise");

let CONSUMER_SECRET = "yYNIn86DMxSiGSarZehUZ"; //need to verify jwt tokens;
let users = {};
let connections = [];

server.listen( 3000, function () {
    let host = server.address().address;
    let port = server.address().port;
    console.log("\n\n---------------------------- " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ----------------------------\n");
    console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + "Im server running at http://" + host + ':' + port);

});


const mysqlCon = mysql.createPool(database.dbSettings());
const mysqlCon2 = mysql2.createPool(database.dbSettings());

let db = function (conn, query, calback) {
    conn.getConnection(function (err, connection) {
        if (err) {
            console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + "error connecting : " + err.stack);
            return;
        }
        console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + "DB connected as id :" + connection.threadId);


        connection.query(query, function (error, results, fields) {
            connection.release();
            if (error) {
                console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " code] " + error.code); // 'ECONNREFUSED'
                console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " fatal] " + error.fatal); // true
            }
            // connected!

            calback(results, error);

        });
    });
};


app.use(express.static(__dirname + '/public'));


app.get("/", function (req, res) {
    res.sendFile(__dirname + "/index.html");

});


function stopServer() {
    console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + "Im server is closed");
    process.exit(0);
}

function clearDisconnectedOldSockets() {
    let query = "TRUNCATE `im_usersocket`";
    db(mysqlCon, query, function (res, err) {
        if (err) {
            console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + "usersocket table clear failed on server stop");
        }
        else {
            console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + "usersocket table clear success on server stop");
            stopServer();
        }
    });
}


process.on("SIGINT", function () {
    clearDisconnectedOldSockets();
});

function deletePendingMessages(g_id, r_id) {
    let query = "DELETE FROM `im_receiver` WHERE g_id=" + mysql.escape(g_id) + " and r_id=" + mysql.escape(r_id);
    db(mysqlCon, query, function (res, err) {
        if (err) {
            console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + "pending message clear failed");
        }
        else {
            console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + "pending message clear success");
        }
    });
}


io.on("connection", function (socket) {

    connections.push(socket);
    users[socket.id] = socket;
    let roomId = null;
    console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + "connected %s", connections.length);


    socket.on("disconnect", function () {

        let connectionIndex = connections.indexOf(socket);
        DeleteSocket(socket.id);
        if (roomId !== null) {
            socket.leave(roomId);
            console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + "leaving room '%s' on disconnect", roomId);
        }

        connections.splice(connectionIndex, 1);
        if (socket.id in users) {
            delete users[socket.id];
        }

        console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + "disconnected %s", connections.length);

    });

    // groupId (int)
    socket.on("joinRoom", function (groupId, userId) {
        roomId = "room-" + groupId;
        socket.join(roomId);
        console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + "joinRoom:" + groupId);

        //deletePendingMessages(parseInt(groupId), parseInt(userId));
    });

    socket.on("leaveRoom", function (groupId) {
        socket.leave("room-" + groupId);
        console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + "leaveRoom:" + groupId);
    });

    socket.on("notTyping", function (responce) {
        let data = null;
        if (typeof responce === "object") {
            data = responce;
        } else {
            data = JSON.parse(responce);
        }
        let tokenData = jwt_decode(data._r);
        let senderData = {
            userName: tokenData.userName,
            profilePicture: tokenData.profilePicture,
            userId: tokenData.userId,
            groupId: data.groupId
        };

        io.sockets.in("room-" + data.groupId).emit("userNotTyping", senderData);
        //console.log("[ "+moment().format('MMMM Do YYYY, hh:mm:ss')+" ] "+"notTyping:" + tokenData.userId);
    });

    socket.on("typing", function (responce) {
        let data = null;
        if (typeof responce === "object") {
            data = responce;
        } else {
            data = JSON.parse(responce);
        }
        let tokenData = jwt_decode(data._r);
        let senderData = {
            userName: tokenData.userName,
            profilePicture: tokenData.profilePicture,
            userId: tokenData.userId,
            groupId: data.groupId
        };
        io.sockets.in("room-" + data.groupId).emit("userTyping", senderData);
        //console.log("[ "+moment().format('MMMM Do YYYY, hh:mm:ss')+" ] "+"typing:" + tokenData.userId);
    });

    socket.on("register", function (responce) {

        try {

            let data = null;
            if (typeof responce === 'object') {
                data = responce;
            } else {
                data = JSON.parse(responce);
            }
            isValidToken(data._r, function (res) {
                if (res) {

                    try {
                        group.setHost(data.url);
                        let user = jwt_decode(data._r);
                        let insertSocketQ = "INSERT INTO `im_usersocket` (`serial`, `userId`, `socketId`) VALUES (NULL, '" + user.userId + "', '" + socket.id + "')";
                        db(mysqlCon, insertSocketQ, function (res, err) {
                            if (err) {
                                console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + "socket id insert failed");

                            }
                            else {
                                console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + "socket id insert success");
                                activeUser(user.userId);
                            }
                        });
                    } catch (err) {
                        console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + err);
                    }

                } else {

                    socket.disconnect();
                    console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + "invalid user");
                }
            });
        } catch (err) {
            console.log(err);
        }

    });

    async function pendingMessage(userId, groupId, lastActive,senderId,recentMessage, messageType) {

        let groupData = await group.get_group(groupId, userId, lastActive, recentMessage, messageType);

        let sendData = {"groupData": groupData,"senderId":senderId};
        let findSocketIdQ = "select socketId from im_usersocket where userId=" + userId;
        db(mysqlCon, findSocketIdQ, function (result, error) {
            if (error) {
                console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + "get socketId failed");
            } else {
                if(result.length>0) {
                    for (let i = 0; i < result.length; i++) {
                        try {

                            users[result[i].socketId].emit("pendingMessage", JSON.stringify(sendData));
                        }
                        catch (err) {
                            DeleteSocket(result[i].socketId);
                            console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + err);
                        }
                    }
                }
            }
        });


    }

    socket.on("addMember", function (res) {
        let data = null;

        if (typeof res === 'object') {
            data = res;
        } else {
            data = JSON.parse(res);
        }
        isValidToken(data._r, function (result) {
            if (result) {
                try {

                    data._r = "";
                    let memberId = data.memberId;
                    if (memberId !== null) {
                        //for (let i = 0; i < members.length; i++) {
                        let findSocketIdQ = "select socketId from im_usersocket where userId=" + memberId;
                        db(mysqlCon, findSocketIdQ, function (result, error) {
                            if (error) {
                                console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + "get socketId failed");
                            } else {
                                for (let i = 0; i < result.length; i++) {
                                    try {

                                        users[result[i].socketId].emit("addNewMember", data);

                                    }
                                    catch (err) {
                                        DeleteSocket(result[i].socketId);
                                        console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + err);
                                    }
                                }
                            }
                        });
                        // }
                    }

                } catch (err) {
                    console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + err);
                }
            } else {
                socket.disconnect();
                console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + "invalid user");
            }


        });
    });

    socket.on("deleteMember", function (res) {
        let data = null;

        if (typeof res === 'object') {
            data = res;
        } else {
            data = JSON.parse(res);
        }
        isValidToken(data._r, function (result) {
            if (result) {
                try {
                    data._r = "";
                    let memberId = data.memberId;
                    if (memberId !== null) {
                        //for (let i = 0; i < members.length; i++) {
                        let findSocketIdQ = "select socketId from im_usersocket where userId=" + memberId;
                        db(mysqlCon, findSocketIdQ, function (result, error) {
                            if (error) {
                                console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + "get socketId failed");
                            } else {
                                for (let i = 0; i < result.length; i++) {
                                    try {
                                        users[result[i].socketId].emit("deleteAMember", data);
                                    }
                                    catch (err) {
                                        DeleteSocket(result[i].socketId);
                                        console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + err);
                                    }
                                }
                            }
                        });
                        // }
                    }

                } catch (err) {
                    console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + err);
                }
            } else {
                socket.disconnect();
                console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + "invalid user");
            }


        });
    });

    socket.on("updateGroupName", function (res) {
        let data = null;

        if (typeof res === 'object') {
            data = res;
        } else {
            data = JSON.parse(res);
        }
        isValidToken(data._r, function (result) {
            if (result) {
                try {
                    data._r = "";
                    let members = data.memberIds;
                    if (members !== null) {
                        for (let i = 0; i < members.length; i++) {
                            let findSocketIdQ = "select socketId from im_usersocket where userId=" + members[i].u_id;
                            db(mysqlCon, findSocketIdQ, function (result, error) {
                                if (error) {
                                    console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + "get socketId failed");
                                } else {
                                    for (let i = 0; i < result.length; i++) {
                                        try {
                                            users[result[i].socketId].emit("updateGroupNameData", data);
                                        }
                                        catch (err) {
                                            DeleteSocket(result[i].socketId);
                                            console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + err);
                                        }
                                    }
                                }
                            });
                        }
                    }

                } catch (err) {
                    console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + err);
                }
            } else {
                socket.disconnect();
                console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + "invalid user");
            }


        });

    });

    async function checkReceiveRecord(m_id,userId,groupid) {
        let receiverQuery = "Select * from `im_receiver` where `g_id`="+groupid+" and `m_id`="+m_id+" and `r_id`="+userId+" and `received`=0";
        let[res,f]=await mysqlCon2.execute(receiverQuery);
        return res.length !== 0;

    }


    socket.on("sendMessage", function (response) {
        let data = null;
        let message = null;
        if (typeof response === 'object') {
            data = response;
        } else {
            data = JSON.parse(response);
        }


    isValidToken(data._r, async function (ret) {
        if (ret) {
            try {
            let res = data.receiversId;
            let receiversRoomId = data.to;
            let availableUsers = [];
            let messageSender=data.sender.userId;
            if (data.message.type !== "update") {
                for (let i = 0; i < res.length; i++) {
                    let uid = res[i].u_id;
                    let findSocketIdQ = "select socketId from im_usersocket where userId=" + uid;
                    let [result, f1] = await mysqlCon2.execute(findSocketIdQ);

                    if (result.length > 0) {
                        for (let i = 0; i < result.length; i++) {
                            try {
                                if (!io.sockets.adapter.sids[result[i].socketId]["room-" + receiversRoomId]) {
                                    if(!await checkReceiveRecord( data.message.m_id,uid,data.to)) {
                                        let receiverQuery = "INSERT INTO `im_receiver` (`serial`,`g_id`, `m_id`, `r_id`, `received`, `time`) VALUES (NULL,'" + data.to + "', '" + data.message.m_id + "', '" + uid + "', '0', '" + data.message.date_time + "');";
                                        await mysqlCon2.execute(receiverQuery);
                                        console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + "message saved to receiver DB");
                                        await pendingMessage(uid, data.to, data.message.ios_date_time, data.sender.userId, data.message.message, data.message.type);
                                    }
                                }
                                else {
                                    if(parseInt(messageSender)!==parseInt(uid)){
                                    let receiverQuery = "INSERT INTO `im_receiver` (`serial`,`g_id`, `m_id`, `r_id`, `received`, `time`) VALUES (NULL,'" + data.to + "', '" + data.message.m_id + "', '" + uid + "', '1', '" + data.message.date_time + "');";
                                    await mysqlCon2.execute(receiverQuery);

                                        availableUsers.push(uid);
                                    }

                                }
                            } catch (app) {
                                console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + app);
                            }
                        }
                    } else {         //user has no active session
                        let receiverQuery = "INSERT INTO `im_receiver` (`serial`,`g_id`, `m_id`, `r_id`, `received`, `time`) VALUES (NULL,'" + data.to + "', '" + data.message.m_id + "', '" + uid + "', '0', '" + data.message.date_time + "');";
                        await mysqlCon2.execute(receiverQuery);

                        console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + "message saved to receiver DB");

                    }

                }
            }
            messageConstructLinkConvert(data, async function (data) {
                message = data;
                message.seen = await group.processSeen(message.message.m_id, receiversRoomId, availableUsers);
                io.sockets.in("room-" + receiversRoomId).emit('newMessage', message);
            });
            } catch (app) {
                console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + app);
            }

        } else {
            socket.disconnect();
            console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + "invalid user");
        }

    });

    });

    socket.on("announceSeen",function (response) {

        let data = null;
        let m_id=null;

        if (typeof response === 'object') {
            data = response;
        } else {
            data = JSON.parse(response);
        }
        m_id=data.recentMessage;

        let query="SELECT sender,receiver FROM `im_message` WHERE m_id="+mysql.escape(m_id);
        db(mysqlCon,query,function (result,error) {
            if(error){
                console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + error);
            }else{
                let senderId=result[0].sender;
                let groupId=result[0].receiver;
                let query="SELECT r_id FROM `im_receiver` WHERE received=1 and m_id="+mysql.escape(m_id);
                db(mysqlCon,query,function (res,err) {
                    if(err){
                        console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + err);
                    }else{
                        let receiversIds=[];
                        for(let i=0;i<res.length;i++){
                            if(res[i].r_id!==senderId){
                                receiversIds.push(res[i].r_id);
                            }
                        }
                        let findSocketIdQ = "select socketId from im_usersocket where userId=" + senderId;
                        db(mysqlCon,findSocketIdQ,async function (r,e) {
                            if(e){
                                console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + e);
                            }else{
                                for (let i=0;i<r.length;i++){
                                    try {
                                        let data={
                                            seen:await group.processSeen(m_id,groupId,receiversIds),
                                            forMessage:m_id
                                        };

                                        users[r[i].socketId].emit("receiveSeen", data);
                                    }
                                    catch (err) {
                                        DeleteSocket(result[i].socketId);
                                        console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + err);
                                    }
                                }
                            }
                        });
                    }
                });
            }
        });

    });
    socket.on("error", function (err) {
        console.log(err);
    });

});

function messageConstructLinkConvert(data, callback) {
    let message = null;
    let mainMessage = data.message.message;
    let mainUrl = null;
    let host = null;
    let title = null;
    let description = null;
    let playerOrImageUrl = null;

    if (hasUrl(mainMessage)) {
        let url = getFirstUrl(mainMessage);
        let responded = false;
        let optionsWihHeader = {
            uri: url,
            timeout: 3000,
            headers: {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.98 Mobile Safari/537.36'},
        };
        let options = {
            uri: url,
            timeout: 3000,
        };

        if (url.match(/@(-?\d+\.\d+),(-?\d+\.\d+),(\d+\.?\d?)+z/g)) { // checking latitude and longitude points are present or not
            options = optionsWihHeader;
        }

        extract(options,
            function (err, res) {
                if (!err) {
                    responded = true;
                    res.url = url;
                    mainUrl = res.url;
                    host = res.host;
                    title = getTitle(res);
                    description = getDescription(res);
                    playerOrImageUrl = getPlayerOrImageUrl(res);
                    let linkData = {
                        mainUrl: mainUrl,
                        host: host,
                        title: title,
                        description: description,
                        playerOrImageUrl: playerOrImageUrl
                    };
                    let linkDataQ = "update `im_message` set `link`=" + mysqlCon.escape(mainUrl) + ",`linkData`=" + mysqlCon.escape(JSON.stringify(linkData)) + " where m_id=" + data.message.m_id;
                    db(mysqlCon, linkDataQ, function (res, err) {
                        if (err) {
                            console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + "Message link data update failed");
                            callback(messageWithOutLink(data));
                        }
                        else {
                            console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + "Message link data update Success");

                            callback(messageWithLink(data, mainUrl, linkData));
                        }
                    });
                } else if (!responded) {
                    responded = true;
                    console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + "Message link fetch failed");
                    callback(messageWithOutLink(data));
                }

            });
    } else {

        callback(messageWithOutLink(data));
    }

}

function hasUrl(message) {
    let surl = getUrl(message);
    let urlArray = Array.from(surl);
    return urlArray.length > 0;
}

function getFirstUrl(message) {
    let surl = getUrl(message, {stripWWW: false});
    let urlArray = Array.from(surl);
    return urlArray[0];

}

function getTitle(res) {
    let mainTitle = null;
    if (res.ogTitle !== undefined) {
        mainTitle = res.ogTitle;
    } else if (res.twitterTitle !== undefined) {
        mainTitle = res.twitterTitle;
    } else if (res.title !== undefined) {
        mainTitle = res.title;
    } else {
        mainTitle = res.host;
    }
    // return mainTitle;
    return mainTitle.replace(/[\n\r]+/g, '').replace(/\s{2,10}/g, ' ').trim();
}

function getDescription(res) {
    let description = null;
    if (res.ogDescription !== undefined) {
        description = res.ogDescription;
    } else if (res.twitterDescription !== undefined) {
        description = res.twitterDescription;
    } else if (res.description !== undefined) {
        description = res.description;
    } else {
        description = ''
    }

    return description.replace(/[\n\r]+/g, '').replace(/\s{2,10}/g, ' ').trim();
    //return description
}

function getPlayerOrImageUrl(res) {
    let url = {url: null, type: null};
    if (res.twitterPlayer !== undefined) {
        url.url = res.twitterPlayer;
        url.type = 'player';
    } else if (res.ogVideoUrl !== undefined) {
        url.url = res.ogVideoUrl;
        url.type = 'player';
    } else if (res.twitterImage !== undefined) {
        url.url = imageUrlFormat(res, res.twitterImage);
        url.type = 'image';
    } else if (res.ogImage !== undefined) {
        url.url = imageUrlFormat(res, res.ogImage);
        url.type = 'image';
    } else if (res.file !== undefined && (res.file.mime === 'image/jpeg' || res.file.mime === 'image/png')) {
        url.url = res.url;
        url.type = 'file';
    }
    return url;
}

function imageUrlFormat(res, imageUrl) {
    let formattedUrl = imageUrl;
    let url = new URL(res.url);
    const urlParts = url2.parse(formattedUrl);
    if (urlParts.host === null) {
        formattedUrl = url.origin + formattedUrl;
    }
    return formattedUrl;
}

function messageWithLink(data, mainUrl, linkData) {
    return {
        "to": data.to,
        "message": {
            "m_id": data.message.m_id,
            "message": data.message.message,
            "type": data.message.type,
            "fileName":data.message.fileName,
            "receiver_type": data.message.receiver_type,
            "date": data.message.date,
            "time": data.message.time,
            // "date_time": data.message.date_time,\
            "link": mainUrl,
            "linkData": JSON.stringify(linkData),
            "ios_date_time": data.message.ios_date_time

        },
        "sender": {
            "userId": data.sender.userId,
            "firstName": data.sender.firstName,
            "lastName": data.sender.lastName,
            "userEmail": data.sender.userEmail,
            "userStatus": data.sender.userStatus,
            "profilePictureUrl": data.sender.profilePictureUrl,
            "active": data.sender.active
        }

    };
}

function messageWithOutLink(data) {
    return {
        "to": data.to,
        "message": {
            "m_id": data.message.m_id,
            "message": data.message.message,
            "type": data.message.type,
            "fileName":data.message.fileName,
            "receiver_type": data.message.receiver_type,
            "date": data.message.date,
            "time": data.message.time,
            // "date_time": data.message.date_time,\
            "link": null,
            "linkData": null,
            "ios_date_time": data.message.ios_date_time

        },
        "sender": {
            "userId": data.sender.userId,
            "firstName": data.sender.firstName,
            "lastName": data.sender.lastName,
            "userEmail": data.sender.userEmail,
            "userStatus": data.sender.userStatus,
            "profilePictureUrl": data.sender.profilePictureUrl,
            "active": data.sender.active
        }

    };
}

function isValidToken(token, callback) {
    try {
        let userSecret = jwt.decode(token, CONSUMER_SECRET).consumerKey;
        let query = "select `userId` from `users` where `userSecret`='" + userSecret + "'";
        db(mysqlCon, query, function (res, err) {
            if (err) {
                console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + err);
                callback(false);
            } else {
                if (res.length === 0) {
                    callback(false);
                }
                else {
                    callback(true);
                }
            }

        });
    } catch (err) {
        console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + err);
        callback(false);
    }

}

function DeleteSocket(socketId) {
    let findUserIdBySocketId = "select DISTINCT userId from `im_usersocket` WHERE socketId='" + socketId + "'";
    db(mysqlCon, findUserIdBySocketId, function (result, err) {
        if (err) {
            console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + "userId fetch failed");
        } else {
            if (result.length > 0) {
                let deleteSocketIdQ = "DELETE FROM `im_usersocket` WHERE  socketId='" + socketId + "'";
                db(mysqlCon, deleteSocketIdQ, function (res, err) {
                    if (err) {
                        console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + "socket id delete failed");
                    }
                    else {
                        console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + "socket id delete success");
                        deactivateUser(result[0].userId);
                    }
                });
            }

        }
    });

}

// Make user online
function activeUser(userId) {
    let updateActiveQuery = "UPDATE `users` SET `active` = 1 WHERE `users`.`userId` = " + userId;
    db(mysqlCon, updateActiveQuery, function (res, err) {
        if (err) {
            console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + "socket user active insert failed");

        } else {

            let selectAllSocketQuery = "select userId, socketId from im_usersocket where userId <>" + userId;
            // friendsSocketQuery for friend list only. replace it with selectAllSocketQuery if you want friendlist based system
            let friendsSocketQuery = "SELECT DISTINCT ims.userId, ims.socketId FROM im_usersocket ims INNER JOIN friend_list fl1 on fl1.userId=ims.userId AND fl1.friendId=" + userId + " WHERE ims.userId<>" + userId;
            db(mysqlCon, selectAllSocketQuery, async function (res, err) {
                if (err) {
                    console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + "user socket id fetch failed");
                }
                else {
                    let activeFriendsId = [];
                    if (res.length > 0) {
                        for (let i = 0; i < res.length; i++) {
                            try {
                            let data = {
                                userId: userId,
                                status: 1,
                                userInfo: await group.get_user(parseInt(userId)),
                            };

                            activeFriendsId.push({
                                userId: res[i].userId,
                                userInfo: await group.get_user(parseInt(res[i].userId)),
                            });


                                users[res[i].socketId].emit("updateStatus", JSON.stringify(data));
                                console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + "socket user status activated");
                            } catch (err) {
                                console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + err);
                            }
                        }


                        let selectUserSocketQuery = "select socketId from im_usersocket where userId=" + userId;
                        db(mysqlCon, selectUserSocketQuery, function (res, err) {
                            if (err) {
                                console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + "user socket id fetch failed");
                            }
                            else {
                                for (let i = 0; i < res.length; i++) {
                                    let data = {
                                        friendsIds: activeFriendsId,
                                        status: 1
                                    };
                                    try {
                                        users[res[i].socketId].emit("updateStatusOnReconnect", JSON.stringify(data));
                                    } catch (err) {
                                        console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + err);
                                    }
                                }
                            }
                        });
                    }
                }
            });


        }
    });

}

// Make user offline
function deactivateUser(userId) {

    let findUserId = "select userId from `im_usersocket` WHERE userId=" + userId;
    db(mysqlCon, findUserId, function (res, err) {
        if (err) {
            console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + "user id deactivation failed");
        }
        else {
            if (res.length === 0) {
                let updateActiveQuery = "UPDATE `users` SET `active` = 0 WHERE `users`.`userId` = " + userId;
                db(mysqlCon, updateActiveQuery, function (res, err) {
                    if (err) {
                        console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + "socket user active insert failed");

                    } else {
                        let selectAllSocketQuery = "select socketId from im_usersocket where userId <>" + userId;
                        // for friend list only
                        let friendsSocketQuery = "SELECT DISTINCT ims.userId, ims.socketId FROM im_usersocket ims INNER JOIN friend_list fl1 on fl1.userId=ims.userId AND fl1.friendId=" + userId + " WHERE ims.userId<>" + userId;
                        db(mysqlCon, selectAllSocketQuery, function (res, err) {
                            if (err) {
                                console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + "user socket id fetch failed");
                            }
                            else {
                                if (res.length > 0) {
                                    for (let i = 0; i < res.length; i++) {
                                        let data = {
                                            userId: userId,
                                            status: 0
                                        };
                                        try {
                                            users[res[i].socketId].emit("updateStatus", JSON.stringify(data));
                                            console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + "socket user status deactivate");
                                        } catch (err) {
                                            console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + err);
                                        }
                                    }
                                }
                            }
                        });
                    }
                });
            }
        }
    });


}