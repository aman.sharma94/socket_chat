"use strict";

const mysql = require("mysql2/promise");
const database = require("./databaseConfig");
const moment = require('moment');
const mysqlCon = mysql.createPool(database.dbSettings());
let baseUrl = null;
let app = {};

app.get_user = async function get_user(u_id) {
    let [userData, f] = await mysqlCon.execute("SELECT * FROM `users` WHERE `userId` = ?", [u_id]);
    let url = baseUrl + "assets/img/download.png";
    if (userData[0].userProfilePicture !== null) {
        url = baseUrl + "assets/userImage/" + userData[0].userProfilePicture;
    }
    return {
        'userId': parseInt(userData[0].userId),
        'firstName': userData[0].firstName,
        'lastName': userData[0].lastName,
        'userEmail': userData[0].userEmail,
        'userAddress': userData[0].userAddress,
        'userMobile': userData[0].userMobile,
        'userStatus': parseInt(userData[0].userStatus),
        'userGender': userData[0].userGender,
        'profilePictureUrl': url,
        'active': parseInt(userData[0].active)
    }
};


app.setHost = (serverHost) => {
    baseUrl = serverHost;
};

app.get_group = async (g_id, u_id, lastActiveTime, recentMessage, messageType) => {
    let membersInfo = [];
    let groupImage = [];
    let [groupInfo, f1] = await mysqlCon.execute("SELECT name FROM `im_group` WHERE `g_id` =? ORDER BY `lastActive` DESC LIMIT 1", [g_id]);
    let lastActive = lastActiveTime;
    let groupName = groupInfo[0].name;
    let [pending, f4] = await mysqlCon.execute("SELECT (CASE WHEN COUNT(m_id) >= 100 THEN 99 ELSE COUNT(m_id) END) as pending FROM `im_receiver` WHERE `r_id` = ? AND `g_id` = ? AND `received` =0 GROUP BY `g_id`", [u_id, g_id]);
    let totalPending = pending[0].pending;
    //let [recentMessage,f2]=await mysqlCon.execute("SELECT * FROM `im_message` WHERE `receiver` = ? ORDER BY `m_id` DESC LIMIT 1",[g_id]);
    let [members, f3] = await mysqlCon.execute("SELECT `u_id` FROM `im_group_members` WHERE `g_id` = ? AND `u_id` <> ?", [g_id, u_id]);
    for (let i = 0; i < members.length; i++) {
        membersInfo.push(await app.get_user(members[i].u_id));
    }
    let [totalMember, f5] = await mysqlCon.execute("SELECT count(u_id) as total FROM `im_group_members` WHERE `g_id` = ?", [g_id]);

    if (totalMember[0].total > 1) {
        if (totalMember[0].total >= 4) {
            for (let i = 0; i < 3; i++) {
                groupImage.push(membersInfo[i].profilePictureUrl);
            }
        } else if (totalMember[0].total >= 3) {
            for (let i = 0; i < 2; i++) {
                groupImage.push(membersInfo[i].profilePictureUrl);
            }
        } else if (totalMember[0].total <= 2) {
            groupImage.push(membersInfo[0].profilePictureUrl);
        }
        totalMember[0].total = totalMember[0].total - 1;
    } else {
        groupImage.push(baseUrl + "assets/img/download.png");
        if (groupName === null || groupName === "" || groupName === '""' || groupName === "''") {
            groupName = "No Member";
        }
    }

    if (groupName === null || groupName === "" || groupName === '""' || groupName === "''" || groupName === "''") {
        groupName = "";
        if (totalMember[0].total <= 2) {
            for (let i = 0; i < totalMember[0].total; i++) {
                if (i === ( totalMember[0].total - 1)) {
                    groupName += " " + membersInfo[i].firstName;
                }
                else {
                    groupName += " " + membersInfo[i].firstName + ",";
                }
            }
        } else if (totalMember[0].total >= 3) {
            for (let i = 0; i < totalMember[0].total; i++) {
                if (i === ( totalMember[0].total - 1)) {
                    groupName += " " + membersInfo[i].firstName;
                }
                else {
                    groupName += " " + membersInfo[i].firstName + ",";
                }
            }
        } else {
            groupName = "No Member";
        }
    }


    return {
        "groupId": parseInt(g_id),
        "groupImage": groupImage,
        "groupName": String(groupName).trim(),
        //"totalMember":totalMember,
        "lastActive": lastActive,
        //"members":membersInfo,
        //"me":me,
        "recentMessage": recentMessage,
        "messageType": messageType,
        "pendingMessage": totalPending
        //"messageDateTime":recentMessage.date_time,
    }


};

app.processSeen=async function (m_id,g_id,receiverIds) {

        let membersIds=receiverIds;
        let [totalMember,f1]=await mysqlCon.execute("SELECT count(u_id) as total FROM `im_group_members` WHERE `g_id` = ?",[g_id]);
        if(membersIds.length===0){
            return null;
        }
        if(totalMember[0].total===2){
            return "Seen";
        }
        if((totalMember[0].total-1)===membersIds.length){
            return "Seen By everyone";
        }
        let names=[];
        for(let i=0;i<membersIds.length;i++){
            let name= await app.get_user(membersIds[i]);
                names.push(String(name.firstName));
        }

         return "Seen By "+names.join(",");

};

app.isReceived=async function (r_id,g_id,m_id) {
    let result= await mysqlCon.execute("SELECT COUNT(*) AS `numrows` FROM `im_receiver` WHERE `g_id` =? AND `r_id` =? AND `m_id` =? AND `received` = 1",[g_id,r_id,m_id]);
    return result[0].length !== 0;

};
module.exports = app;
