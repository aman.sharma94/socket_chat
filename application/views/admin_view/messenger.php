<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<section class="page-contents">

        <div class="leftSection col-xl-3 col-md-3 col-sm-12 col-xs-12" style="padding: 0;">
            <div class="chat-search col-md-12" id=""  style="">
                <div class="form-group col-md-12 col-sm-12 col-xs-12" id="">
                   <div class="col-md-12" style="padding: 0;font-size: 18px;font-weight: bold"><span class="" style="padding: 0;sty"></span>Group List </div>
                </div>
            </div>
            <div style="float:left; width: 100%"  id="groupDiv">
                <ul class="persons" id="groups" >

                </ul>
            </div>
        </div>
        <div class="middleSection col-xl-6 col-md-6 col-sm-12 col-xs-12" style="padding: 0;border-left: 1px solid rgba(0, 0, 0, .10)">
           <div class="chat-search col-md-12 groupNameDiv" style="text-align: left; padding-bottom: 21px;">
               <div class="col-md-10 col-xl-10 col-xs-10 col-sm-10">
                    <h3 style="font-size: 18px;"><span class="UserNames"  style="display: inline-block;white-space: nowrap; overflow: hidden;text-overflow: ellipsis;width: 95%"></span></h3>
               </div>

           </div>


            <div class="chat col-md-12 col-xl-12 col-sm-12 col-xs-12" style="overflow: scroll;overflow-x: hidden;" id="chatBox" >


            </div>




        </div>

        <div class="rightSection col-xl-3 col-md-3 col-sm-12 col-xs-12 persons2 " style="padding: 0;border-left: 1px solid rgba(0, 0, 0, .10);border-top: 1px solid rgba(0, 0, 0, .10); text-overflow: ellipsis;overflow-x: hidden;overflow-y: hidden ">
            <div class="col-md-12 col-xl-12 col-xs-12 col-sm-12 text-center  person2 rightGroupImages" style="padding-top: 5px;display: flex;justify-content: center;"   >
            </div>
            <div class="col-md-12 col-xl-12 col-xs-12 col-sm-12 text-center pad-10" >
                <h4 class="be-use-name" style="text-transform: uppercase; overflow: hidden; text-overflow: ellipsis"></h4>
            </div>
            <div class="col-md-12 col-xl-12 col-xs-12 col-sm-12 text-center pad-2" style="padding-bottom: 5px" >
                <div class="preview be-user-info" style="font-size: 10px; padding-bottom:5px;border-bottom:1px solid rgba(0, 0, 0, .10) "></div>
            </div>


                <ul class="persons personsList" id="groupMembers" >



                </ul>


        </div>


</section>
<!-- Modals -->





<script type="text/javascript" src="<?php echo base_url("assets/newTheme/assets/js/loadingoverlay.js")."?".rand(5,50000); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/newTheme/assets/js/loadingoverlay_progress.js")."?".rand(5,50000); ?>"></script>

<script type="text/javascript">
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-bottom-left",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };

    $(document).ready(function () {
        $("body").css({"overflow-y":"hidden"});
        $(window).bind("resize",function () {
            var viewHeight=$(window).height();
            var viewWidth=$(window).width();
            if(viewWidth<990){
                $('#convStart').css("height",75);
                $('.persons').css({"margin-top":0});
                $(".rightSection").css({'margin-top': '16px'});
                $(".groupNameDiv").css({"padding-bottom":'32px'});
                $('.video').css({'margin-left': '-34px'});

            }
            else {
                $(".rightSection").css({'margin-top': '0px'});
                $(".groupNameDiv").css({"padding-bottom":'21px'});
                $('.video').css({'margin-left': '0px'})
            }
            if(viewWidth<990){
                $(".leftSection").css({"height":(viewHeight-95)});
                $(".middleSection").css({"height":(viewHeight-95)});
                $(".rightSection").css({"height":(viewHeight-95)});
            }
            else{
                $(".leftSection").css({"height":540});
                $(".middleSection").css({"height":540});
                $(".rightSection").css({"height":540});
            }
            $(".chat").css({"height":(viewHeight-220)});
            $('.persons').css({"height":(viewHeight-160)});
            $('.personsList').css({"height":(viewHeight-250)});
        }).trigger("resize");



        var chatBox=$('#chatBox');

        var videoObjects=[];


        var responce=localStorage.getItem("_r");
        var type=jwt_decode(responce).userType;
        var start=0;
        var limit=10;
        var totalRetivedMessage=0;
        var chatsData=[]; //used as stack. reverse the chat message on scroll
        var userId=<?php echo $userId ?>;
        function increaseStart() {
            start+=limit;
        }
        function resetStart() {
            start=0;
        }

        function resetRetiveMessage(){
            totalRetivedMessage=0;
        }


        if(responce!=null && responce!='' && type==0)
        {


            getGroupList(function (data) {
                if(data){
                    $("#groups li").first().trigger("click",[{update:true}]);
                }
            });
        }
        else if(type==1)
        {
            location.href="<?php echo base_url("/userview")  ?>";
        }
        else {
            location.href="<?php echo base_url()  ?>";
        }
        var groupIds=[];
        var time=[];
        var groupImages={};
        function printGroupList(groups){
            var html="";
            groupIds=[];

            time={};
            for(i=0;i<groups.length;i++){
                groupIds.push(groups[i].groupId);
                time[groups[i].groupId]=groups[i].lastActive;
                html += " <li class=\"person\" data-chat=\"person1\" id='group_"+groups[i].groupId+"' data-group=\""+groups[i].groupId+"\">";
                for (j=0;j<groups[i].groupImage.length;j++){
                    groupImages[groups[i].groupId]=groups[i].groupImage;
                    html += "                        <img class=\"img-responsive img-circle\" style=\"width: 40px; height: 40px;border-radius: 50%\" src=\""+groups[i].groupImage[j]+"\" >";
                }
                html += "                        <span class=\"name\" style=\"overflow: hidden\"><div>"+groups[i].groupName+"</div><\/span>";
                var date=moment(groups[i].lastActive,moment.ISO_8601).fromNow();

                html += "                        <span id='time_"+groups[i].groupId+"' class=\"time\">"+date+"<\/span>";
                if(groups[i].messageType=="text"){
                    html += "                        <span style='float: left' id='messageType_"+groups[i].groupId+"' class=\"preview\">"+groups[i].recentMessage+"<\/span>";
                }else{
                    html += "                        <span style='float: left' id='messageType_"+groups[i].groupId+"' class=\"preview\">"+groups[i].messageType+"<\/span>";
                }
                html += "                        <div style='width: 26px;margin-left: 275px;' id='notice_"+groups[i].groupId+"' class=\"pad-2 notice hidden text-center-md text-center-lg text-center-sm\" ><\/div>";
                html += "                    <\/li>";
            }
            $("#groups").html(html);
        }

        function getLinkPreview(linkData,link){
            if(linkData.playerOrImageUrl.type==='player'){
                return "<div class=''><iframe src='"+linkData.playerOrImageUrl.url+"' class='medea-frame iframe-wrapper' allowfullscreen></iframe></div>";
            }
            else {
                if(linkData.playerOrImageUrl.url!=null){
                    let image = "<img src='" + linkData.playerOrImageUrl.url + "' id='tImg' width='100%'>";
                    return "<div class='linkPreview-wrapper'><a href='" + link + "' target=\"_blank\"><div id='texts'><div id='thumbnail' >" + image + "</div><div><div id='title'><div>" + linkData.title + "</div></div> <div id='desc'><div class='clamp-desc'>" + linkData.description + "</div> <div id='meta'><div id='domain'>" + linkData.host + "</div><div class='clear'></div></div></div></div></div></a></div>";
                }
                return "<div class='linkPreview-wrapper'><a href='" + link + "' target=\"_blank\"><div id='texts'><div><div id='title'><div>" + linkData.title + "</div></div> <div id='desc'><div class='clamp-desc'>" + linkData.description + "</div> <div id='meta'><div id='domain'>" + linkData.host + "</div><div class='clear'></div></div></div></div></div></a></div>";
            }
        }

        function parseMessage(message) {
            return  twemoji.parse(
                anchorme(message,{
                    truncate:[15,10],
                    attributes:[
                        function(urlObj){
                            if(urlObj.protocol !== "mailto:")
                                return {name:"target",value:"blank"};
                        }
                    ]
                })
            );
        }

        function getGroupList(callback){
            var settings = {
                "async": true,
                "crossDomain": true,
                "url": "<?php echo base_url('adminApi/getGroups/') ?>?userId="+userId,
                "method": "GET",
                "headers": {
                    "authorization": "Basic YWRtaW46MTIzNA==",
                    "Authorizationkeyfortoken": String(responce),
                    "cache-control": "no-cache",
                    "postman-token": "eb27c011-391a-0b70-37c5-609bcd1d7b6d"
                },
                "processData": false,
                "contentType": false,

            };

            $.ajax(settings).done(function (response) {

                var groups=response.response;

                if(groups.length<=0){
                    $('#addMember').attr('data-group',null);
                    $('#addMember').addClass("hidden");
                    chatBox.html('<img id="blankImg" src="<?php echo base_url('assets/img/nomess.png')?>" class="img-responsive" style="width:500px">');
                    chatBox.addClass("text-center");
                    $('#groupMembers').html("");
                    $('#groups').html('');
                    $("#editGroupName").addClass("hidden");
                    $('.UserNames').html('');
                }else{
                    $('#addMember').removeClass("hidden");
                    $("#editGroupName").removeClass("hidden");
                    printGroupList(groups);
                    // $("#groups li").first().trigger("click");
                    if(callback!=null|| callback!=""){
                        if(groups.length>0){
                            callback(true);
                        }else {
                            callback(false);
                        }

                    }else {
                        $("#groups li").first().trigger("click",[{update:true}]);
                    }
                }



            });

        }

        function printGroupInfo(groupId,groupImages,groupName){
            var html="";
            var images=groupImages[groupId];
            for(i=0;i<images.length;i++){
                html += "                        <img class=\"img-responsive img-circle\" style=\"width: 40px; height: 40px;border-radius: 50%\" src=\""+images[i]+"\" >";
            }
            $('.rightGroupImages').html(html);
            $('.be-use-name').html(groupName);
        }

        $('#groups').on("click",".person",function (e,update) {

            $('#chatBox').perfectScrollbar('destroy');
            for(i=0;i<videoObjects.length;i++){
                videoObjects[i].dispose();
            }
            videoObjects=[];
            resetStart();
            resetRetiveMessage();
            if ($(this).hasClass('active')) {
                return false;
            }
            if ($('#addMember').hasClass('hidden')) {
                $('#addMember').removeClass('hidden');
            }
            var groupId = $(this).attr('data-group');
            var personName = $(this).find('.name').text();

            $('.UserNames').html(personName);
            $('.person').removeClass('active');
            $(this).addClass('active');
            var oldGroupId=$('#addMember').attr('data-group');
            $('#addMember').attr('data-group',groupId);
            $('#editGroupName').attr('data-group',groupId);
            var updateList=true;
            if(typeof update!='undefined' ){
                updateList=update.update;
            }
            if(updateList){
                getGroupMembers(groupId);
            }


            clearChatBox();
            getMessage(groupId,start,limit);


            printGroupInfo(groupId,groupImages,personName);


        });

        function printGroupMembers(members,meCreator,groupId) {
            var html="";
            for (i=0;i<members.length;i++){
                html += "<li class=\"person\"  style=\"padding-top: 5px;padding-bottom: 22px;cursor: default;\">";
                html += "                        <img src=\""+members[i].profilePictureUrl+"\" alt=\"\" \/>";
                html += "                        <span  class=\"name\"><div style='margin-top: 8px'>"+members[i].firstName+" "+members[i].lastName +"</div><\/span>";
                html += "                    <\/li>";
            }
            $('#groupMembers').html(html);
        }
        function getGroupMembers(groupId) {
            var settings = {
                "async": true,
                "crossDomain": true,
                "url": "<?php echo base_url('adminApi/getMembers?groupId=') ?>"+groupId+"&userId="+userId,
                "method": "GET",
                "headers": {
                    "authorization": "Basic YWRtaW46MTIzNA==",
                    "Authorizationkeyfortoken": String(responce),
                    "cache-control": "no-cache",
                    "postman-token": "eb27c011-391a-0b70-37c5-609bcd1d7b6d"
                },
                "processData": false,
                "contentType": false
            };
            $.ajax(settings).done(function (response) {
                var members=response.response.memberList;
                var meCreator=response.response.meCreator;
                printGroupMembers(members,meCreator,groupId);
            });

        }



        function getMembers(callback) {
            var settings={
                "async": true,
                "crossDomain": true,
                "url": "<?php echo base_url('user/friendList/') ?>",
                "method": "GET",
                "headers": {
                    "authorization": "Basic YWRtaW46MTIzNA==",
                    "Authorizationkeyfortoken": String(responce),
                    "cache-control": "no-cache",
                    "postman-token": "eb27c011-391a-0b70-37c5-609bcd1d7b6d"
                },
                "dataType" : 'json'
            };
            $.ajax(settings).done(function (response) {

                var data=response.response;
                callback(data);
            });
        }


        function clearChatBox() {
            chatBox.html('');
        }
        var scrollPosition=null;
        function getMessage(groupId) {
            if(start==1){
                start=0;
            }
            var url="<?php echo base_url('adminApi/getMessage?groupId=') ?>"+groupId+"&limit="+limit+"&start="+start;

            var settings={
                "async": true,
                "crossDomain": true,
                "url": url,
                "method": "GET",
                "headers": {
                    "authorization": "Basic YWRtaW46MTIzNA==",
                    "Authorizationkeyfortoken": String(responce),
                    "cache-control": "no-cache",
                    "postman-token": "eb27c011-391a-0b70-37c5-609bcd1d7b6d"
                },
                "processData": false,
                "contentType": false

            };
            $.ajax(settings).done(function (result) {

                var data=result.response;
                var html="";
                totalRetivedMessage+=data.length;

                if(data.length==0){
                    chatBox.html('<img id="blankImg" src="<?php echo base_url('assets/img/nomess.png')?>" class="img-responsive" style="width:500px">');
                    chatBox.addClass("text-center");
                }else{
                    chatBox.removeClass("text-center");
                    for(i=0;i<data.length;i++){

                        var sender=data[i].sender;
                        var message=data[i].message;

                        var senderId=data[i].sender.userId;
                        if(senderId==userId){
                            html += "<div class=\"fw-im-message  fw-im-isme fw-im-othersender\" data-og-container=\"\">";
                            if(message.type=="text"){
                                html += "                    <div id='message_"+message.m_id+"' class=\"fw-im-message-text\">"+parseMessage(message.message)+"<\/div>";
                                if(message.linkData!=null){
                                    html+=getLinkPreview(JSON.parse(message.linkData),message.link);
                                }
                            }
                            if(message.type=="image"){
                                html += "<div id='message_"+message.m_id+"' class=\"fw-im-attachments\"><a style=\"width: 200px;height: 200px\" href=\""+message.message+"\" class=\"ol-hover hover-5 ol-lightbox\"><img height=\"200px\" width=\"200px\" src=\""+message.message+"\" alt=\"image hover\">";
                                html += "                            <div class=\"ol-overlay ov-light-alpha-80\"><\/div>";
                                html += "                            <div class=\"icons\"><i class=\"fa fa-camera\"><\/i><\/div><\/a>";
                                html += "                            <\/div>";
                            }
                            if(message.type=="video"){
                                html += "<div id='message_"+message.m_id+"' class=\"fw-im-attachments\" >";
                                html += "                        <video id='video_"+message.m_id+"' class='video-js ' width=\"250px\" height=\"150px\" controls=\"true\" preload='none' name=\"media\"><source src=\""+message.message+"\" type=\"video\/mp4\"><\/video>";
                                html += "                    <\/div>";
                            }
                            if(message.type=="audio"){
                                html += "<div id='message_"+message.m_id+"' class=\"fw-im-attachments\" >";
                                html += "                        <audio id='audio_"+message.m_id+"' class='video-js  vjs-default-skin' width=\"250px\" height=\"150px\" controls=\"true\" preload='none' name=\"media\"><source src=\""+message.message+"\" type=\"audio\/mp3\"><\/audio>";
                                html += "                    <\/div>";
                            }
                            html += "                    <div class=\"fw-im-message-author\">";
                            html += "                        <img src=\""+sender.profilePictureUrl+"\" title=\""+sender.firstName+" "+sender.lastName+"\">";
                            html += "                    <\/div>";
                            html += "                    <div class=\"fw-im-message-time\">";
                            html += "                        <span style=\"cursor:help\" title=\""+moment(message.ios_date_time,moment.ISO_8601).format('LLLL')+"\">"+moment(message.ios_date_time,moment.ISO_8601).calendar()+"<\/span>";
                            html += "                    <\/div>";
                            html += "                <\/div>";
                        }
                        else{
                            html += "                <div class=\"fw-im-message  fw-im-isnotme fw-im-othersender\" data-og-container=\"\">";
                            if(message.type=="text"){
                                html += "                    <div id='message_"+message.m_id+"' class=\"fw-im-message-text\">"+parseMessage(message.message)+"<\/div>";
                                if(message.linkData!=null){

                                    html+=getLinkPreview(JSON.parse(message.linkData),message.link);
                                }
                            }
                            if(message.type=="image"){
                                html += "<div id='message_"+message.m_id+"' class=\"fw-im-attachments\"><a style=\"width: 200px;height: 200px\" href=\""+message.message+"\" class=\"ol-hover hover-5 ol-lightbox\"><img height=\"200px\" width=\"200px\" src=\""+message.message+"\" alt=\"image hover\">";
                                html += "                            <div class=\"ol-overlay ov-light-alpha-80\"><\/div>";
                                html += "                            <div class=\"icons\"><i class=\"fa fa-camera\"><\/i><\/div><\/a>";
                                html += "                            <\/div>";
                            }
                            if(message.type=="video"){
                                html += "<div id='message_"+message.m_id+"' class=\"fw-im-attachments\">";
                                html += "                        <video id='video_"+message.m_id+"' class='video-js' width=\"250px\" height=\"150px\" controls=\"true\" preload='none'  name=\"media\"><source src=\""+message.message+"\" type=\"video\/mp4\"><\/video>";
                                html += "                    <\/div>";
                            }
                            if(message.type=="audio"){
                                html += "<div id='message_"+message.m_id+"' class=\"fw-im-attachments\" >";
                                html += "                        <audio id='audio_"+message.m_id+"' class='video-js  vjs-default-skin' width=\"250px\" height=\"150px\" controls=\"true\" preload='none' name=\"media\"><source src=\""+message.message+"\" type=\"audio\/mp3\"><\/audio>";
                                html += "                    <\/div>";
                            }
                            html += "                    <div class=\"fw-im-message-author\">";
                            html += "                        <img src=\""+sender.profilePictureUrl+"\" title=\""+sender.firstName+" "+sender.lastName+"\">";
                            html += "                    <\/div>";
                            html += "                    <div class=\"fw-im-message-time\">";
                            html += "                        <span style=\"cursor:help\" title=\""+moment(message.ios_date_time,moment.ISO_8601).format('LLLL')+"\">"+moment(message.ios_date_time,moment.ISO_8601).calendar()+"<\/span>";
                            html += "                    <\/div>";
                            html += "                <\/div>";
                        }


                    }

                    chatBox.html("");

                    chatBox.append(html);
                    chatBox.scrollTop(0);

                    for(i=0;i<data.length;i++){
                        var allMessage=data[i].message;
                        if(allMessage.type=="video"){
                            videoObjects.push(videojs("video_"+allMessage.m_id, {}, function(){
                                    // Player (this) is initialized and ready.
                                })
                            );
                        }else if(allMessage.type=="audio"){
                            videoObjects.push(videojs("audio_"+allMessage.m_id, {}, function(){
                                    // Player (this) is initialized and ready.
                                })
                            );
                        }
                    }

                    var height=chatBox[0].scrollHeight;
                    scrollPosition=height;
                    //chatBox.scrollTop( chatBox.prop( "scrollHeight" ) );
                    chatBox.scrollTop(height);

                    $('#notice_'+groupId).addClass("hidden");
                    lightBox.init();
                    chatBox.perfectScrollbar();
                    $('.clamp-desc').each(function (index,element) {
                        $clamp(element, {clamp: 3, useNativeClamp: false});
                    });

                }


            });

        }


        var notRequested=true;
        chatBox.on("ps-scroll-up",function() {

            if (notRequested && chatBox.scrollTop()==0) {
                notRequested=false;
                increaseStart();

                var groupId= $('#addMember').attr('data-group');
                var url="<?php echo base_url('imApi/getMessage?groupId=') ?>" + groupId + "&limit="+limit+"&start=" + start;


                var settings = {
                    "async": true,
                    "crossDomain": true,
                    "url": url,
                    "method": "GET",
                    "headers": {
                        "authorization": "Basic YWRtaW46MTIzNA==",
                        "Authorizationkeyfortoken": String(responce),
                        "cache-control": "no-cache",
                        "postman-token": "eb27c011-391a-0b70-37c5-609bcd1d7b6d"
                    },
                    "processData": false,
                    "contentType": false,
                    "beforeSend":function () {
                        chatBox.prepend("<div class='loader'></div>");
                    },
                    "complete":function () {
                        $('.loader').hide();

                    }
                };
                $.ajax(settings).done(function (result) {
                    notRequested=true;
                    var data = result.response;

                    if(totalRetivedMessage==result.totalMessage){
                        resetStart();
                        return;
                    }
                    var html = "";
                    for (i = 0; i < data.length; i++) {
                        var self = data[i].self;
                        var sender = data[i].sender;
                        var message = data[i].message;

                        var senderId=data[i].sender.userId;
                        if(senderId==userId){
                            html += "<div  class=\"fw-im-message  fw-im-isme fw-im-othersender\" data-og-container=\"\">";
                            if (message.type == "text") {
                                html += "                    <div id='message_"+message.m_id+"' class=\"fw-im-message-text\">" +parseMessage(message.message) + "<\/div>";
                                if(message.linkData!=null){
                                    html+=getLinkPreview(JSON.parse(message.linkData),message.link);
                                }
                            }
                            if (message.type == "image") {
                                html += "<div id='message_"+message.m_id+"' class=\"fw-im-attachments\"><a style=\"width: 200px;height: 200px\" href=\"" + message.message + "\" class=\"ol-hover hover-5 ol-lightbox\"><img height=\"200px\" width=\"200px\" src=\"" + message.message + "\" alt=\"image hover\">";
                                html += "                            <div class=\"ol-overlay ov-light-alpha-80\"><\/div>";
                                html += "                            <div class=\"icons\"><i class=\"fa fa-camera\"><\/i><\/div><\/a>";
                                html += "                            <\/div>";
                            }
                            if (message.type == "video") {
                                html += "<div id='message_"+message.m_id+"' class=\"fw-im-attachments\" >";
                                html += "                        <video id='video_"+message.m_id+"' class='video-js ' width=\"250px\" height=\"150px\" controls=\"true\" preload='none'  name=\"media\"><source src=\"" + message.message + "\" type=\"video\/mp4\"><\/video>";
                                html += "                    <\/div>";
                            }
                            if (message.type == "audio") {
                                html += "<div id='message_"+message.m_id+"' class=\"fw-im-attachments\" >";
                                html += "                        <audio id='audio_"+message.m_id+"' class='video-js ' width=\"250px\" height=\"150px\" controls=\"true\" preload='none' name=\"media\"><source src=\"" + message.message + "\" type=\"audio\/mp3\"><\/audio>";
                                html += "                    <\/div>";
                            }

                            html += "                    <div class=\"fw-im-message-author\">";
                            html += "                        <img src=\"" + sender.profilePictureUrl + "\" title=\"" + sender.firstName + " " + sender.lastName + "\">";
                            html += "                    <\/div>";
                            html += "                    <div class=\"fw-im-message-time\">";
                            html += "                        <span style=\"cursor:help\" title=\"" + moment(message.ios_date_time,moment.ISO_8601).format('LLLL') + "\">" + moment(message.ios_date_time,moment.ISO_8601).calendar() + "<\/span>";
                            html += "                    <\/div>";
                            html += "                <\/div>";
                        }
                        else {
                            html += "                <div class=\"fw-im-message  fw-im-isnotme fw-im-othersender\" data-og-container=\"\">";
                            if (message.type == "text") {
                                html += "                    <div id='message_"+message.m_id+"' class=\"fw-im-message-text\">" + parseMessage(message.message) + "<\/div>";
                                if(message.linkData!=null){
                                    html+=getLinkPreview(JSON.parse(message.linkData),message.link);
                                }
                            }
                            if (message.type == "image") {
                                html += "<div id='message_"+message.m_id+"' class=\"fw-im-attachments\"><a style=\"width: 200px;height: 200px\" href=\"" + message.message + "\" class=\"ol-hover hover-5 ol-lightbox\"><img height=\"200px\" width=\"200px\" src=\"" + message.message + "\" alt=\"image hover\">";
                                html += "                            <div class=\"ol-overlay ov-light-alpha-80\"><\/div>";
                                html += "                            <div class=\"icons\"><i class=\"fa fa-camera\"><\/i><\/div><\/a>";
                                html += "                            <\/div>";
                            }
                            if (message.type == "video") {
                                html += "<div id='message_"+message.m_id+"' class=\"fw-im-attachments\">";
                                html += "                        <video id='video_"+message.m_id+"' class='video-js ' width=\"250px\" height=\"150px\" controls=\"true\"  preload='none' name=\"media\"><source src=\"" + message.message + "\" type=\"video\/mp4\"><\/video>";
                                html += "                    <\/div>";
                            }
                            if (message.type == "audio") {
                                html += "<div id='message_"+message.m_id+"' class=\"fw-im-attachments\" >";
                                html += "                        <audio id='audio_"+message.m_id+"' class='video-js ' width=\"250px\" height=\"150px\" controls=\"true\" preload='none'  name=\"media\"><source src=\"" + message.message + "\" type=\"audio\/mp3\"><\/audio>";
                                html += "                    <\/div>";
                            }
                            html += "                    <div class=\"fw-im-message-author\">";
                            html += "                        <img src=\"" + sender.profilePictureUrl + "\" title=\"" + sender.firstName + " " + sender.lastName + "\">";
                            html += "                    <\/div>";
                            html += "                    <div class=\"fw-im-message-time\">";
                            html += "                        <span style=\"cursor:help\" title=\"" + moment(message.ios_date_time,moment.ISO_8601).format('LLLL') + "\">" + moment(message.ios_date_time,moment.ISO_8601).calendar() + "<\/span>";
                            html += "                    <\/div>";
                            html += "                <\/div>";
                        }

                    }
                    totalRetivedMessage+=data.length;

                    chatBox.prepend(html);
                    for(i=0;i<data.length;i++){
                        var allMessage=data[i].message;
                        if(allMessage.type=="video"){
                            videoObjects.push(videojs("video_"+allMessage.m_id, {}, function(){
                                    // Player (this) is initialized and ready.
                                })
                            );
                        }else if(allMessage.type=="audio"){
                            videoObjects.push(videojs("audio_"+allMessage.m_id, {}, function(){
                                    // Player (this) is initialized and ready.
                                })
                            );
                        }
                    }

                    // var height=chatBox[0].scrollHeight;

                    chatBox.scrollTop(scrollPosition);
                    lightBox.init();
                    $('.loader').hide();
                    $('.clamp-desc').each(function (index,element) {
                        $clamp(element, {clamp: 3, useNativeClamp: false});
                    });

                });
            }
        });



        var captureImage = function(file) {
            var canvas = document.createElement("canvas");
            canvas.width = 40;
            canvas.height = 40;

            canvas.strokeStyle = 'black';
            canvas.lineWidth = 1;
            canvas.getContext('2d').strokeRect(0, 0, canvas.width, canvas.height);
            canvas.getContext('2d').drawImage(file, 0, 0, canvas.width-1, canvas.height-1);

            var img = document.getElementById("fileIV");
            img.src = canvas.toDataURL("image/png");
            //$output.prepend(img);
        };
        var captureImagenewMessage = function(file) {
            var canvas = document.createElement("canvas");
            canvas.width = 40;
            canvas.height = 40;

            canvas.strokeStyle = 'black';
            canvas.lineWidth = 1;
            canvas.getContext('2d').strokeRect(0, 0, canvas.width, canvas.height);
            canvas.getContext('2d').drawImage(file, 0, 0, canvas.width-1, canvas.height-1);

            var img = document.getElementById("newMessagefileIV");
            img.src = canvas.toDataURL("image/png");
            //$output.prepend(img);
        };

        $('#newMessagefileIV').on("click",function () {
            $("#newMessageFile").click();
        });

        $('#fileIV').on("click",function () {
            $("#messageFile").click();
        });

        function imageChange(event) {
            var file = this.files[0];
            var imagefile = file.type;
            var size=file.size;
            var match= ["image/jpeg","image/png","image/jpg","video/3gpp","video/mp4","video/3gp","audio/mp3"];
            if(size>20971520){
                toastr.error("Max limit 20Mb exceeded");
                return ;
            }

            if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2]) || (imagefile==match[3]) || (imagefile==match[4]) || (imagefile==match[5])||(imagefile==match[6])))
            {
                toastr.error("This type of file is not allowed");
                return false;
            }else {
                $('#sendMessage').trigger('click');


            }
        }
        function imageChangeNewMessage(event) {
            var file = this.files[0];
            var imagefile = file.type;
            var size=file.size;
            var match= ["image/jpeg","image/png","image/jpg","video/3gpp","video/mp4","video/3gp","audio/mp3"];
            if(size>20971520){
                toastr.error("Max limit 20Mb exceeded");
                return ;
            }

            if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2]) || (imagefile==match[3]) || (imagefile==match[4]) || (imagefile==match[5]) || (imagefile==match[6])))
            {
                toastr.error("This type of file is not allowed");
                return false;
            }else {

                $('#newSendMessage').trigger('click');


            }
        }

        $("#messageFile").change(imageChange);
        $("#newMessageFile").change(imageChangeNewMessage);




        $('#groups').perfectScrollbar();
        $('#groupMembers').perfectScrollbar();
        chatBox.perfectScrollbar();
        function updateTime() {
            for (i=0;i<groupIds.length;i++){
                var date=moment(time[groupIds[i]],moment.ISO_8601).fromNow();
                $('#time_'+groupIds[i]).html(date);
            }

        }
        setInterval(updateTime, 10000);
    });


</script>
</body>
</html>