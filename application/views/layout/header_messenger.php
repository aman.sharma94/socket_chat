<?php
/**
 * Created by PhpStorm.
 * User: Farhad Zaman
 * Date: 12/20/2016
 * Time: 2:03 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- globalise scripting and styling content language-->
    <meta name="Content-Type-Script" content="text/javascript">
    <meta name="Content-Type-Style" content="text/css">
    <!-- author of this doc-->
    <meta name="author" content="Codejamers">
    <!-- ################################-->
    <!-- #### add SEO metadata here-->
    <!-- ################################-->
    <!-- #####Begin styles-->
    <!-- stylesheets	-->
    <link href="<?php echo base_url("assets/newTheme/assets2/reset.css") ?>" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url("assets/css/bootstrap-multiselect.css") ?>">
    <link href="<?php echo base_url("assets/newTheme/assets/css/vendors/vendors.css") ?>" rel="stylesheet" type="text/css">
    <!-- Load extra page specific css-->
    <!-- Overwrite vendors-->
    <link href="<?php echo base_url("assets/newTheme/assets/css/vendors/vendors-overwrites.css") ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url("assets/newTheme/assets/css/styles.css") ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url("assets/newTheme/assets/css/fastselect.min.css") ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url("assets/newTheme/assets/css/magicsuggest.css") ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url("assets/newTheme/assets2/custom2.css")."?".rand(5,50000); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url("assets/newTheme/assets/css/toastr.min.css") ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url("assets/newTheme/assets/css/dataTables.bootstrap.min.css") ?>" rel="stylesheet" type="text/css">
    <!-- #####End styles-->
    <!-- #####Begin JS-->
    <!-- add your scripts to the end of the page for sake of page load speed-->
    <!-- scripts that need to be at head section-->
    <script src="<?php echo base_url("assets/newTheme/assets/js/vendors/jquery.min.js") ?>"></script>
    <!-- #####End JS-->
    <!-- #####Begin load google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Play:400,700&amp;subset=latin,greek,cyrillic" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Sintony:400,700&amp;subset=latin,greek,cyrillic" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Merriweather:300,300italic,400,400italic,700,700italic&amp;
    subset=latin,greek,cyrillic" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url("assets/css/bootstrap-timepicker.min.css") ?>">
    <link rel="stylesheet" href="<?php echo base_url("assets/css/jquery-ui.min.css") ?>">
    <link href="//vjs.zencdn.net/5.4.6/video-js.min.css" rel="stylesheet">

    <!-- #####Begin JS-->
    <!-- add your scripts to the end of the page for sake of page load speed-->
    <!-- scripts that need to be at head section-->
    <!-- #####End JS-->
    <!-- #####Begin load google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Play:400,700&amp;subset=latin,greek,cyrillic" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Sintony:400,700&amp;subset=latin,greek,cyrillic" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Merriweather:300,300italic,400,400italic,700,700italic&amp;subset=latin,greek,cyrillic" rel="stylesheet" type="text/css">
    <!-- #####End load google fonts-->
    <title>IM MESSENGER</title>
    <link rel="shortcut icon" href="<?php echo base_url('assets/img/pp.png') ?>">
    <!-- #####Begin scripts-->
    <script src="<?php echo base_url("assets/newTheme/assets/js/vendors/vendors.js")."?".rand(5,50000); ?>"></script>
    <!-- Local Revolution tools-->
    <!-- Only for local and can be removed on server-->

    <script src="<?php echo base_url("assets/newTheme/assets/js/custom.js") ?>"></script>
    <script src="<?php echo base_url("assets/newTheme/assets/js/jquery-dateFormat.min.js") ?>"></script>
    <script src="<?php echo base_url("assets/newTheme/assets/js/jquery.playSound.js") ?>"></script>
    <script src="<?php echo base_url("assets/newTheme/assets/js/toastr.min.js") ?>"></script>
    <script src="<?php echo base_url("assets/newTheme/assets/js/anchorme.min.js") ?>"></script>
    <script src="<?php echo base_url("assets/newTheme/assets/js/fastselect.standalone.min.js") ?>"></script>
    <script src="<?php echo base_url("assets/newTheme/assets/js/magicsuggest.js")."?".rand(5,50000); ?>"></script>
    <script src="<?php echo base_url("assets/newTheme/assets/js/jquery.dataTables.min.js") ?>"></script>
    <script src="<?php echo base_url("assets/newTheme/assets/js/dataTables.bootstrap.min.js") ?>"></script>
    <script src="<?php echo base_url("assets/newTheme/assets/js/moment.min.js") ?>"></script>
    <script src="<?php echo base_url("assets/newTheme/assets/js/clamp.min.js") ?>"></script>
    <!--<script src="<?php /*echo base_url("assets/newTheme/assets/js/socket.io.min.js")."?".rand(5,50000); */?>"></script>
    <script src="<?php /*echo base_url("assets/newTheme/assets/js/socket.io-file-client.js")."?".rand(5,50000); */?>"></script>
    <script src="<?php /*echo base_url("assets/newTheme/assets/js/si.js")."?".rand(5,50000); */?>"></script>-->
    <script src="<?php echo base_url("assets/js/scripts/jwt-decode.min.js") ?>"></script>
    <script src="<?php echo base_url("assets/js/bootstrap-timepicker.min.js") ?>"></script>
    <script src="<?php echo base_url("assets/js/jquery-ui.min.js") ?>"></script>
    <script src="<?php echo base_url("assets/js/bootstrap-multiselect.js") ?>"></script>

    <script src="//vjs.zencdn.net/5.4.6/video.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url("assets/newTheme/assets/js/twemoji-picker.js") ?>"></script>
    <script type="text/javascript" src="<?php echo base_url("assets/newTheme/assets/js/perfect-scrollbar.jquery.js") ?>"></script>
    <!--<script type="text/javascript" src="<?php /*echo base_url("assets/newTheme/twemoji/2/twemoji.min.js")."?".rand(5,50000) */?>"></script>-->
    <script src="//twemoji.maxcdn.com/2/twemoji.min.js?2.2.3"></script>
    <link href="<?php echo base_url("assets/newTheme/assets/css/twemoji-picker.min.css") ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url("assets/newTheme/assets/css/perfect-scrollbar.css") ?>" rel="stylesheet" type="text/css">
    <script src="<?php echo base_url("assets/js/video.min.js")?>"></script>
    <script src="<?php echo base_url("assets/js/scripts/fullcalendar.min.js") ?>"></script>
</head>
