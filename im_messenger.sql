-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 21, 2018 at 06:02 PM
-- Server version: 5.7.20-log
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `im_messenger`
--
CREATE DATABASE IF NOT EXISTS `im_messenger` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `im_messenger`;

-- --------------------------------------------------------

--
-- Table structure for table `admingroup`
--

CREATE TABLE `admingroup` (
  `id` int(10) UNSIGNED NOT NULL,
  `adminType` int(11) DEFAULT NULL,
  `adminInfo` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admingroup`
--

INSERT INTO `admingroup` (`id`, `adminType`, `adminInfo`) VALUES
(1, 0, 'superUser'),
(2, 1, 'subUser'),
(3, 2, 'supersuperUser');

-- --------------------------------------------------------

--
-- Table structure for table `admintype`
--

CREATE TABLE `admintype` (
  `id` int(10) UNSIGNED NOT NULL,
  `adminId` int(11) DEFAULT NULL,
  `adminType` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admintype`
--

INSERT INTO `admintype` (`id`, `adminId`, `adminType`) VALUES
(4, 7, 2);

-- --------------------------------------------------------

--
-- Table structure for table `admin_contactinfo`
--

CREATE TABLE `admin_contactinfo` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(500) DEFAULT NULL,
  `phone` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_contactinfo`
--

INSERT INTO `admin_contactinfo` (`id`, `email`, `phone`) VALUES
(1, 'mybaby@mybaby.com', '01671175657');

-- --------------------------------------------------------

--
-- Table structure for table `friend_list`
--

CREATE TABLE `friend_list` (
  `serial` int(10) UNSIGNED NOT NULL,
  `userId` int(11) DEFAULT NULL,
  `friendId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `friend_list`
--

INSERT INTO `friend_list` (`serial`, `userId`, `friendId`) VALUES
(4, 4, 5),
(5, 4, 6),
(6, 5, 4),
(7, 6, 4),
(8, 5, 10),
(9, 10, 5),
(10, 5, 11),
(11, 11, 5);

-- --------------------------------------------------------

--
-- Table structure for table `im_group`
--

CREATE TABLE `im_group` (
  `g_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `type` tinyint(1) DEFAULT '0' COMMENT '1=fixed,0=float',
  `lastActive` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `im_group`
--

INSERT INTO `im_group` (`g_id`, `name`, `createdBy`, `type`, `lastActive`) VALUES
(1, 'Global', 10, 1, '2018-01-08 09:39:32'),
(2, NULL, 10, 1, '2018-01-07 10:12:35'),
(3, NULL, 10, 1, '2018-01-21 15:49:12'),
(4, NULL, 10, 0, '2018-01-21 08:24:43'),
(5, NULL, 10, 0, '2018-01-08 09:53:29'),
(6, NULL, 10, 0, '2018-01-21 09:07:15'),
(7, NULL, 10, 0, '2018-01-21 08:23:25'),
(8, NULL, 10, 0, '2018-01-21 13:33:20'),
(9, NULL, 10, 0, '2018-01-21 13:33:43'),
(10, NULL, 10, 0, '2018-01-21 13:34:28');

-- --------------------------------------------------------

--
-- Table structure for table `im_group_members`
--

CREATE TABLE `im_group_members` (
  `serial` int(10) UNSIGNED NOT NULL,
  `g_id` int(11) DEFAULT NULL,
  `u_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `im_group_members`
--

INSERT INTO `im_group_members` (`serial`, `g_id`, `u_id`) VALUES
(2, 1, 10),
(4, 2, 10),
(7, 3, 10),
(9, 4, 10),
(18, 5, 10),
(20, 6, 10),
(22, 7, 10),
(24, 8, 10),
(27, 3, 5),
(28, 3, 11),
(34, 6, 11),
(35, 7, 5),
(36, 6, 5),
(39, 9, 10),
(40, 10, 11),
(41, 10, 10);

-- --------------------------------------------------------

--
-- Table structure for table `im_message`
--

CREATE TABLE `im_message` (
  `m_id` int(10) UNSIGNED NOT NULL,
  `sender` int(11) DEFAULT NULL,
  `receiver` int(11) DEFAULT NULL,
  `message` longtext,
  `type` varchar(20) DEFAULT NULL,
  `fileName` longtext COMMENT 'real file Name for audio,video,image,attachments',
  `link` varchar(500) DEFAULT NULL,
  `linkData` longtext,
  `receiver_type` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `time` time DEFAULT NULL,
  `date_time` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
--
-- Dumping data for table `im_message`
--


-- --------------------------------------------------------

--
-- Table structure for table `im_receiver`
--

CREATE TABLE `im_receiver` (
  `serial` int(11) UNSIGNED NOT NULL,
  `g_id` int(11) UNSIGNED NOT NULL,
  `m_id` int(11) UNSIGNED NOT NULL,
  `r_id` int(11) UNSIGNED NOT NULL,
  `received` int(1) NOT NULL,
  `time` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `im_receiver`
--



-- --------------------------------------------------------

--
-- Table structure for table `im_usersocket`
--

CREATE TABLE `im_usersocket` (
  `serial` int(11) UNSIGNED NOT NULL,
  `userId` int(11) UNSIGNED NOT NULL,
  `socketId` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `im_usersocket`
--


-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `version` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`version`) VALUES
(11);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `userId` int(11) UNSIGNED NOT NULL,
  `userSecret` varchar(255) NOT NULL,
  `firstName` varchar(100) NOT NULL,
  `lastName` varchar(100) DEFAULT NULL,
  `userEmail` varchar(100) NOT NULL,
  `userPassword` varchar(255) NOT NULL,
  `userMobile` varchar(30) DEFAULT NULL,
  `userDateOfBirth` timestamp NULL DEFAULT NULL,
  `userGender` varchar(15) DEFAULT NULL,
  `userStatus` int(11) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=inactive,1=active',
  `userVerification` int(11) DEFAULT NULL,
  `userAddress` varchar(255) DEFAULT NULL,
  `userProfilePicture` varchar(255) DEFAULT NULL,
  `userResetToken` varchar(255) DEFAULT NULL,
  `userType` int(11) NOT NULL,
  `userRole` int(2) NOT NULL,
  `lastModified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userId`, `userSecret`, `firstName`, `lastName`, `userEmail`, `userPassword`, `userMobile`, `userDateOfBirth`, `userGender`, `userStatus`, `active`, `userVerification`, `userAddress`, `userProfilePicture`, `userResetToken`, `userType`, `userRole`, `lastModified`) VALUES
(5, 'fX5mHGD7UW', 'anisul', 'hoque', 'anis@gmail.com', '$2y$10$.qRXVQTWkTe2oJTOb/UJbeI9DqcfXyTE4VURlFwrOWuk0u68otW3S', NULL, NULL, NULL, 1, 1, 1, NULL, '0611201773348profile.jpg', NULL, 1, 1, '2017-04-05 20:47:40'),
(7, 'MdE9UegmMV', 'admin', '', 'admin@admin.com', '$2y$10$.qRXVQTWkTe2oJTOb/UJbeI9DqcfXyTE4VURlFwrOWuk0u68otW3S', NULL, NULL, NULL, 1, 0, 1, NULL, NULL, NULL, 0, 1, '2017-04-05 20:48:20'),
(10, 'btZSmNftbZ', 'simon', 'hasan', 'simon@gmail.com', '$2y$10$.qRXVQTWkTe2oJTOb/UJbeI9DqcfXyTE4VURlFwrOWuk0u68otW3S', NULL, NULL, NULL, 1, 1, 1, NULL, '1030201775416profile10.jpg', NULL, 1, 0, '2017-10-16 18:33:04'),
(11, 'gsdkUwg5Er', 'Hasan', 'Zaman', 'hasan@gmail.com', '$2y$10$CN7oFFzwps4llUCculFz5ungWhdN91LTvrfXVZA0ydkVNnPz/s66W', NULL, NULL, NULL, 1, 1, 1, NULL, NULL, NULL, 1, 0, '2017-10-29 20:49:55');

-- --------------------------------------------------------

--
-- Table structure for table `users_roles`
--

CREATE TABLE `users_roles` (
  `type` int(2) UNSIGNED NOT NULL,
  `role` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_roles`
--

INSERT INTO `users_roles` (`type`, `role`) VALUES
(0, 'ROLE_ADMIN'),
(1, 'ROLE_USER');

-- --------------------------------------------------------

--
-- Table structure for table `user_device`
--

CREATE TABLE `user_device` (
  `serial` int(11) UNSIGNED NOT NULL,
  `userId` int(11) UNSIGNED NOT NULL,
  `deviceId` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admingroup`
--
ALTER TABLE `admingroup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admintype`
--
ALTER TABLE `admintype`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_contactinfo`
--
ALTER TABLE `admin_contactinfo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `friend_list`
--
ALTER TABLE `friend_list`
  ADD PRIMARY KEY (`serial`);

--
-- Indexes for table `im_group`
--
ALTER TABLE `im_group`
  ADD PRIMARY KEY (`g_id`),
  ADD KEY `createdBy` (`createdBy`);

--
-- Indexes for table `im_group_members`
--
ALTER TABLE `im_group_members`
  ADD PRIMARY KEY (`serial`);

--
-- Indexes for table `im_message`
--
ALTER TABLE `im_message`
  ADD PRIMARY KEY (`m_id`);

--
-- Indexes for table `im_receiver`
--
ALTER TABLE `im_receiver`
  ADD PRIMARY KEY (`serial`);

--
-- Indexes for table `im_usersocket`
--
ALTER TABLE `im_usersocket`
  ADD PRIMARY KEY (`serial`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`userId`);

--
-- Indexes for table `users_roles`
--
ALTER TABLE `users_roles`
  ADD PRIMARY KEY (`type`);

--
-- Indexes for table `user_device`
--
ALTER TABLE `user_device`
  ADD PRIMARY KEY (`serial`),
  ADD KEY `userId` (`userId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admingroup`
--
ALTER TABLE `admingroup`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `admintype`
--
ALTER TABLE `admintype`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `admin_contactinfo`
--
ALTER TABLE `admin_contactinfo`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `friend_list`
--
ALTER TABLE `friend_list`
  MODIFY `serial` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `im_group`
--
ALTER TABLE `im_group`
  MODIFY `g_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `im_group_members`
--
ALTER TABLE `im_group_members`
  MODIFY `serial` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `im_message`
--
ALTER TABLE `im_message`
  MODIFY `m_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=393;

--
-- AUTO_INCREMENT for table `im_receiver`
--
ALTER TABLE `im_receiver`
  MODIFY `serial` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=464;

--
-- AUTO_INCREMENT for table `im_usersocket`
--
ALTER TABLE `im_usersocket`
  MODIFY `serial` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `userId` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `user_device`
--
ALTER TABLE `user_device`
  MODIFY `serial` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `user_device`
--
ALTER TABLE `user_device`
  ADD CONSTRAINT `user_device_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `users` (`userId`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
